<?php

require __DIR__.'/vendor/autoload.php';

use Advent\Commands\Day1;
use Advent\Commands\Day2;
use Advent\Commands\Day3;
use Advent\Commands\Day4;
use Advent\Commands\Day6;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new Day1());
$application->add(new Day2());
$application->add(new Day3());
$application->add(new Day4());
$application->add(new Day6());

$application->run();
