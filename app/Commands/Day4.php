<?php namespace Advent\Commands;

use Advent\Days\Day4\Room;
use Illuminate\Support\Collection;

class Day4 extends Day
{

    public function getCommandName()
    {
        return 'day4';
    }

    public function executeCommand($input, $output)
    {
        $lines = $this->getLinesFromFile('input/day4.txt');
        foreach ($lines as $line) {
            $rooms[] = new Room($line);
        }
        $roomsCollection = new Collection($rooms);
        $valid = $roomsCollection->filter(function (Room $room) {
            return $room->isValid();
        });
        if ($this->isPart1()) {
            $this->output(
                "Sum of sectors: ".
                $valid->sum(function(Room $room) {
                    return $room->getSector();
                })
            );
        } else {
            $valid->filter(function (Room $room) {
                return (strpos($room->getShiftedName(), 'pole') !== false);
            })->each(function (Room $room) {
               $this->output($room->getShiftedName().': '.$room->getSector());
            });
        }
    }
}
