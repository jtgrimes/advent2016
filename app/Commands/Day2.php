<?php namespace Advent\Commands;

use Advent\Days\Day2\FancyPad;
use Advent\Days\Day2\NumberPad;

class Day2 extends Day
{

    public function getCommandName()
    {
        return 'day2';
    }

    public function executeCommand($input, $output)
    {
        if ($this->isPart1()) {
            $pad = new NumberPad(5);
        } else {
            $pad = new FancyPad(5);
        }

        $lines = $this->getLinesFromFile('input/day2.txt');
        foreach ($lines as $line) {
            $press[] = $pad->processLine($line);
            $this->output('Press '.end($press));
        }

        $this->output("Code is ".implode('', $press));

    }
}
