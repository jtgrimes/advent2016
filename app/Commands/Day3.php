<?php namespace Advent\Commands;

class Day3 extends Day
{
    public function getCommandName()
    {
        return 'day3';
    }

    public function executeCommand($input, $output)
    {
        $valid = 0;
        $lines = $this->getLinesFromFile('input/day3.txt');
        foreach ($lines as $line) {
            $values = preg_split('/[\s,]+/', $line);
            asort($values);
            if ($values[0] + $values[1] > $values[2]) {
                $valid++;
            }
        }

        $this->output("Count is $valid");
    }
}
