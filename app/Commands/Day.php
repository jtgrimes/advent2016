<?php namespace Advent\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Day extends Command
{
    protected $outputStream;
    protected $inputStream;

    abstract public function getCommandName();

    protected function configure()
    {
        $this->setName($this->getCommandName())
            ->setDescription('Advent of code 2016')
            ->addArgument('part', InputArgument::OPTIONAL, 'Part 1 or Part 2?', 1);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->outputStream = $output;
        $this->inputStream = $input;
        $this->executeCommand($input, $output);
    }

    abstract public function executeCommand($input, $output);

    protected function isPart1()
    {
        return ($this->inputStream->getArgument('part') == 1);
    }

    protected function isPart2()
    {
        return ($this->inputStream->getArgument('part') == 2);
    }
    protected function output($string, $ifVerbose = false)
    {
        $this->outputStream->writeln(
            $string,
            $ifVerbose ? OutputInterface::VERBOSITY_VERBOSE : OutputInterface::VERBOSITY_NORMAL
        );
    }

    protected function getLinesFromFile($filename)
    {
        $lines = [];
        $handle = fopen($filename, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $lines[] = $line;
            }

            fclose($handle);
        } else {
            throw new \Exception("Could not open file");
        }
        return $lines;
    }
}
