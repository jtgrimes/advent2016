<?php namespace Advent\Commands;

use Advent\Days\Day6\Decoder;

class Day6 extends Day
{

    public function getCommandName()
    {
        return 'day6';
    }

    public function executeCommand($input, $output)
    {
        $lines = $this->getLinesFromFile('input/day6.txt');
        $decoder = new Decoder($lines);
        if ($this->isPart1()) {
            $this->output('Decoded message: '.$decoder->decode('max'));
        } else {
            $this->output('Decoded message: '.$decoder->decode('min'));
        }

    }
}
