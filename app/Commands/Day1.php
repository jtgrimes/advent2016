<?php namespace Advent\Commands;

use Advent\Days\Day1\Walker;

class Day1 extends Day
{
    public function getCommandName()
    {
        return "day1";
    }

    public function executeCommand($input, $output)
    {
        $walker = new Walker();

        $inputText = "R4, R5, L5, L5, L3, R2, R1, R1, L5, R5, R2, L1, L3, L4, R3, L1, L1, R2, R3, R3, R1, L3, L5, R3, R1, L1, R1, R2, L1, L4, L5, R4, R2, L192, R5, L2, R53, R1, L5, R73, R5, L5, R186, L3, L2, R1, R3, L3, L3, R1, L4, L2, R3, L5, R4, R3, R1, L1, R5, R2, R1, R1, R1, R3, R2, L1, R5, R1, L5, R2, L2, L4, R3, L1, R4, L5, R4, R3, L5, L3, R4, R2, L5, L5, R2, R3, R5, R4, R2, R1, L1, L5, L2, L3, L4, L5, L4, L5, L1, R3, R4, R5, R3, L5, L4, L3, L1, L4, R2, R5, R5, R4, L2, L4, R3, R1, L2, R5, L5, R1, R1, L1, L5, L5, L2, L1, R5, R2, L4, L1, R4, R3, L3, R1, R5, L1, L4, R2, L3, R5, R3, R1, L3";
        $moves = $this->parseMoves($inputText);
        foreach ($moves as $move) {
            $direction = substr($move, 0, 1);
            $distance = substr($move, 1);
            $this->output("Direction: $direction, Distance: $distance", true);
            $position = $walker->move($direction, $distance);
            $this->output(sprintf("Position: %d, %d", $position['latitude'], $position['longitude']), true);
        }
        if ($this->isPart1()) {
            $this->output(sprintf("Final distance: %d", $walker->getDistanceTravelled()));
        } else {
            $firstDuplicatePosition = $walker->getFirstDuplicate();
            $this->output(sprintf(
                "First duplicate: %d %d",
                $firstDuplicatePosition['latitude'],
                $firstDuplicatePosition['longitude']
            ));
            $distance = abs($firstDuplicatePosition['latitude'])+abs($firstDuplicatePosition['longitude']);
            $this->output("Total distance: $distance");
        }
    }

    private function parseMoves($input)
    {
        return explode(', ', $input);
    }
}
