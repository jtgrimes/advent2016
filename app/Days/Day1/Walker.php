<?php namespace Advent\Days\Day1;

class Walker
{
    const NORTH = 0;
    const EAST = 1;
    const SOUTH = 2;
    const WEST = 3;

    private $direction;
    private $lat;
    private $long;

    private $visited;
    private $firstVisited;

    public function __construct()
    {
        $this->direction = self::NORTH;
        $this->lat = 0;
        $this->long = 0;
        $this->markPositionVisited();
    }

    public function move($direction, $distance)
    {
        $this->turn($direction);
        $this->walk($distance);
        return $this->getPosition();
    }

    public function getPosition()
    {
        return ['latitude' => $this->lat, 'longitude' => $this->long];
    }

    public function getDistanceTravelled()
    {
        return abs($this->lat) + abs($this->long);
    }

    private function turn($direction)
    {
        if ($direction == 'L') {
            $this->direction = ($this->direction +4 -1) % 4; // adding 4 so that we don't end up negative
            return;
        }
        $this->direction = ($this->direction +1) % 4;
    }

    private function walk($distance)
    {
        for ($i=1; $i<=$distance; $i++) {
            switch ($this->direction) {
                case self::NORTH:
                    $this->long += 1;
                    break;
                case self::SOUTH:
                    $this->long -= 1;
                    break;
                case self::EAST:
                    $this->lat += 1;
                    break;
                case self::WEST:
                    $this->lat -= 1;
                    break;
            }
            $this->markPositionVisited();
        }
    }

    private function markPositionVisited()
    {
        if (isset($this->visited[$this->lat][$this->long])) {
            $this->visited[$this->lat][$this->long] += 1;
            if (!isset($this->firstVisited)) {
                $this->firstVisited = ['latitude' => $this->lat, 'longitude' => $this->long];
            }
        } else {
            $this->visited[$this->lat][$this->long] = 1;
        }
    }

    public function getFirstDuplicate()
    {
        return $this->firstVisited;
    }
}
