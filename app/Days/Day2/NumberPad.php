<?php namespace Advent\Days\Day2;

class NumberPad
{
    protected $position;

    public function __construct($startingPosition)
    {
        $this->position = $startingPosition;
    }

    public function processLine($line)
    {
        $moves = str_split($line);
        foreach ($moves as $direction) {
            $this->move($direction);
        }
        return $this->position;
    }

    public function move($direction)
    {
        switch ($direction) {
            case 'U':
                if ($this->position > 3) {
                    $this->position = $this->position - 3;
                }
                break;
            case 'D':
                if ($this->position < 7) {
                    $this->position = $this->position + 3;
                }
                break;
            case 'L':
                if ($this->position % 3 !== 1) {
                    $this->position = $this->position - 1;
                }
                break;
            case 'R':
                if ($this->position % 3 !== 0) {
                    $this->position = $this->position + 1;
                }
                break;
        }
    }
}
