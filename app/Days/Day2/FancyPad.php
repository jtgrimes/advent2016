<?php namespace Advent\Days\Day2;

class FancyPad extends NumberPad
{
    public function move($direction)
    {
        switch ($direction) {
            case 'U':
                switch ($this->position) {
                    case '3':
                        $this->position = 1;
                        break;
                    case '6':
                        $this->position = 2;
                        break;
                    case '7':
                        $this->position = 3;
                        break;
                    case '8':
                        $this->position = 4;
                        break;
                    case 'A':
                        $this->position = 6;
                        break;
                    case 'B':
                        $this->position = 7;
                        break;
                    case 'C':
                        $this->position = 8;
                        break;
                    case 'D':
                        $this->position = 'B';
                        break;
                }
                break;
            case 'D':
                switch ($this->position) {
                    case '1':
                        $this->position = 3;
                        break;
                    case '2':
                        $this->position = 6;
                        break;
                    case '3':
                        $this->position = 7;
                        break;
                    case '4':
                        $this->position = 8;
                        break;
                    case '6':
                        $this->position = 'A';
                        break;
                    case '7':
                        $this->position = 'B';
                        break;
                    case '8':
                        $this->position = 'C';
                        break;
                    case 'B':
                        $this->position = 'D';
                        break;
                }
                break;
            case 'L':
                switch ($this->position) {
                    case '3':
                        $this->position = 2;
                        break;
                    case '4':
                        $this->position = 3;
                        break;
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        $this->position = $this->position - 1;
                        break;
                    case 'B':
                        $this->position = 'A';
                        break;
                    case 'C':
                        $this->position = 'B';
                        break;
                }
                break;
            case 'R':
                switch ($this->position) {
                    case '2':
                    case '3':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                        $this->position = $this->position + 1;
                        break;
                    case 'A':
                        $this->position = 'B';
                        break;
                    case 'B':
                        $this->position = 'C';
                        break;
                }
                break;
        }
    }
}
