<?php namespace Advent\Days\Day4;

use Illuminate\Support\Collection;

class Room
{
    protected $name;
    protected $sector;
    protected $checksum;

    protected $regex = '/(.*)-(\d*)\[(\w*)\]/';
    public function __construct($input)
    {
        $matches = [];
        preg_match($this->regex, $input, $matches);
        $this->name = $matches[1];
        $this->sector = $matches[2];
        $this->checksum = $matches[3];
    }

    public function isValid()
    {
        return ($this->checksum == $this->calculateChecksum());
    }

    private function calculateChecksum()
    {
        $letters = str_split($this->name);
        $counts = array_count_values($letters);
        unset($counts['-']);
        arsort($counts);
        // so now counts is descending, but we still have to alphabetize equal letters
        // this is gonna get a little crazy...
        $numberCounts = array_count_values($counts);
        $finalSort = [];

        $holding = [];
        foreach ($counts as $letter => $count) {
            if ($numberCounts[$count] == 1) { // this one is unique -- use it.
                $finalSort[] = $letter;
            } else {
                $holding[] = $letter;
                if (count($holding) == $numberCounts[$count]) {
                    // we have everything in holding -- let's do the dew
                    asort($holding);
                    $finalSort = array_merge($finalSort, $holding);
                    $holding=[];
                }
            }
            if (count($finalSort) >= 5) {
                break;
            }
        }
        return implode('', array_slice($finalSort, 0, 5));
    }

    public function getSector()
    {
        return $this->sector;
    }

    public function getShiftedName()
    {
        $shift = $this->sector % 26;
        $letters = new Collection(str_split($this->name));
        $newName = implode('', $letters->map(function($letter) use ($shift) {
            if ($letter == '-') return ' ';
            return $this->shiftLetter($letter, $shift);
        })->toArray());
        return $newName;

    }

    private function shiftLetter($letter, $shift)
    {
        $ord = ord($letter) + $shift;
        if ($ord > 122) $ord = $ord - 26;
        return chr($ord);

    }
}
