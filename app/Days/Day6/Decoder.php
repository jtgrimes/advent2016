<?php namespace Advent\Days\Day6;

class Decoder
{
    private $lines;
    public function __construct(array $lines)
    {
        $this->lines = $lines;
    }

    public function decode($mode = 'max')
    {
        $working = '';
        $columns = $this->convertToColumns();
        foreach ($columns as $column) {
            $count = array_count_values($column);
            if ($mode == 'max') {
                arsort($count);
            } else {
                asort($count);
            }
            reset($count); // who knew? https://secure.php.net/manual/en/function.reset.php
            $working .= key($count);
        }
        return $working;
    }

    private function convertToColumns()
    {
        $columns = [];
        foreach ($this->lines as $line) {
            $row = str_split($line);
            foreach ($row as $index=>$letter) {
                $columns[$index][] = $letter;
            }
        }
        return $columns;
    }
}
